# znic

A 1-page webzine generator. It outputs a single HTML file from a nested directory of text and images.

- Converts vanilla Markdown to HTML, while plain text is inserted as preformatted text
- Embeds images as base64-encoded strings
- Builds a table of contents
- Uses a basic HTML template to customise the webzine appearance
- Supported text formats: `.md .txt`
- Supported image formats: `.gif .jpg .png .svg`

Example: [tilde town zine #7] (27 M)


## Build

- Install [Nim] for your distribution.

- Build: `nim c -d:release znic.nim`


## Usage

1. Create a new config directory in the top level of the path with the source files: `znic new [path]`

2. The `.znic` config directory contains a sample config and HTML theme file. Edit these to suit project needs.

3. For each article, add a `.metadata.json` in the same subdirectory as the source files. All fields are optional, though it would be best to add at least the title and author to be displayed properly in the table of contents and article header.

```
{
  "title": "Article Title",
  "author": "Article Author",
  "showTitles": true,
  "inlineImages": false
}
```
  - `showTitles`: enables display of titles for each item in a series.
  - `inlineImages`: enables the option to inline images between text in a custom Markdown file (see section below).

4. It is recommended to have a source directory structure that looks something like this:

```
[path]/
  |- .znic
  |   |- project.json
  |   |- theme.html
  |   |- header.png (optional)
  |- article1/
  |   |- .metadata.json
  |   |- article.md
  |- article2/
  |   |- .metadata.json
  |   |- article.md
  |- article3/
  |   |- .metadata.json
      |- article.md
      |- image1.png
      |- image2.png
```

5. Generate the webzine — it will create an `index.html` file at the top level of the directory: `znic make [path]`


### Custom Markdown file

It is possible to make basic customisations to the placement of images in a text by creating a `.metatext.md` file in the same location as the source files and inserting delimiter tags indicating where images should appear. For example:

```
{= style =}
.article3-image { text-align: center; }
{= /style =}

## Images

This is the first image.

{= div class="article3-image" =}{! image1.png !}{= /div =}

This is another image.

{= div class="article3-image" =}{! image2.png !}{= /div =}
```

To insert the images into inline CSS styles as background images, use `{!! image.ext !!}` instead:

```
{= style =}
.bg { background-image: {!! image1.png !!}; }
{= /style =}
```


## License

[0BSD](LICENSE)


[Nim]: https://nim-lang.org/
[tilde town zine #7]: https://tilde.town/~zine/issues/7/
