import packages/docutils/[rst, rstgen]
import std/[algorithm, base64, htmlgen, json, jsonutils, options, os, parseopt,
  sequtils, strtabs, strutils, sugar, unicode]


type
  Url = tuple
    class: string
    href: string
    name: string
    source: string
    title: string

  SourceMeta = object
    title: Option[string]
    author: Option[string]
    showTitles: Option[bool]
    inlineImages: Option[bool]

  Source = tuple
    anchor: string
    author: string
    content: string
    ext: string
    first: bool
    inlineImages: bool
    last: bool
    name: string
    path: string
    serial: bool
    series: string
    showTitles: bool
    title: string

  Config = tuple
    title: string
    description: string
    nav: seq[Url]
    header: string
    footer: string
    ignoreDirs: seq[string]
    ignoreFiles: seq[string]


const
  app = (
    name: "znic",
    version: "0.1.2",
    source: "https://codeberg.org/mio/znic",
  )

  env = (
    configDir: "." & app.name,
    configFile: "project.json",
    themeFile: "theme.html",
    headerFile: "header",
    metaFile: ".metadata.json",
    metaText: ".metatext.md",
    metaHtml: ".meta.html",
    outputFile: "index.html",
    ext: (
      image: @[".gif", ".jpg", ".png", ".svg"],
      text: @[".html", ".md", ".txt"],
    ),
  )

  err = (
    dirNotExists: "Error: the directory does not exist.",
    configNotFound: "Error: config files not found. " &
      "Please create new ones first.",
    configNotParsed: "Error: config files could not be parsed. " &
      "Please check the config settings.",
    fileNotLoaded: "Error: file could not be loaded.",
    fileNotSaved: "Error: file could not be saved.",
    jsonNotLoaded: "Error: JSON could not be loaded. " &
      "Please check the file exists and contains valid syntax.",
  )

  help = @[
    (cmd: "new", params: "[path]", desc: "Output a new project config"),
    (cmd: "make", params: "[path]", desc: "Generate the HTML file"),
    (cmd: "version", params: "", desc: "Show the app version"),
  ]

  theme = (
    css: (
      article: "article",
      articles: "features",
      articleAuthor: "article-author",
      articleHeader: "article-header",
      articleImg: "article-img",
      articleImgSvg: "article-img-svg",
      articleNav: "article-nav",
      articleTitle: "article-title",
      articlePreTxt: "article-pre",
      articleTxt: "article-txt",
      footer: "footer",
      header: "header",
      main: "content",
      nav: "nav",
      navAnchor: "nav",
      navLink: "nav-link",
      toc: "toc",
      tocAnchor: "toc",
      tocItem: "toc-item",
      tocItemAuthor: "toc-item-author",
      tocItemTitle: "toc-item-title",
      tocLink: "toc-link",
      tocTitle: "toc-title",
    ),
    exp: (
      app: "{{ app }}",
      articles: "{{ articles }}",
      description: "{{ description }}",
      footer: "{{ footer }}",
      header: "{{ header }}",
      nav: "{{ nav }}",
      title: "{{ title }}",
      toc: "{{ toc }}",
    ),
    label: (
      articleNav: "",
      nav: "Back to top",
      toc: "Contents",
      tocTitle: "Contents",
    ),
    image: (
      inlineStart: "{! ",
      inlineEnd: " !}",
      bgStart: "{!! ",
      bgEnd: " !!}",
    ),
  )

  defConfig: Config = (
    title: "My New Zine",
    description: "An awesome html zine",
    nav: @[
      (
        class: theme.css.navLink,
        href: app.source,
        name: app.source,
        source: "znic",
        title: "go to the " & app.name & " project page",
      ),
    ],
    header: "My New Zine",
    footer: "made with ♥ & " & app.name,
    ignoreDirs: @[],
    ignoreFiles: @[
      ".metadata.json",
      ".metatext.md",
    ],
  )

  defThemeCss = """
* {
  border: 0;
  margin: 0;
  padding: 0;
}

:root {
  --dark: #111;
  --dark-grey: #333;
  --light: #fefefe;
  --light-grey: #eee;
  --mono: Hack, Courier Prime, Courier, monospace;
  --sans: Open Sans, Helvetica, sans-serif;
}

body {
  background: var(--light);
  color: var(--dark);
  font-family: var(--sans);
}

.nav {
  height: 2.5rem;
  max-width: 100%;
  text-align: center;
}
.nav .nav-link {
  height: 2rem;
  position: relative;
  top: 0.5rem;
}

.header {
  text-align: center;
}
.header .article-img {
  max-height: calc(100vh - 2rem);
}

.footer {
  margin: 1rem auto;
  text-align: center;
}

.toc {
  margin: 5rem auto;
  text-align: center;
}
.toc-title {
  margin: 3rem 0;
}
.toc-item {
  margin: 2rem 0;
}
.toc-item-title, .toc-item-author {
  list-style-type: none;
}
.toc-item-title {
  font-style: italic;
}

.features {
  max-width: 100%;
  padding: 2rem 0;
}

.article {
  margin: 2rem auto;
  max-width: 70%;
}

.article-nav {
  font-size: 0.9rem;
  margin: 5rem 0;
  text-align: right;
}
.article-nav .nav-link {
  margin: 0 0.5rem 0 0;
}

.article-header {
  margin: 7rem 0 2rem;
}
.article-title {
  font-size: 2rem;
  margin: 0;
}
.article-author {
  font-size: 1.3rem;
  margin: 0.5rem 0 0;
}

.article-img {
  max-width: 80%;
  max-height: 100vh;
}
.article-img-svg {
  max-width: 100%;
}

.article-pre {
  font-family: var(--mono);
  font-size: 0.9rem;
  white-space: pre-wrap;
  word-wrap: break-word;
}

.article-txt {
  margin: 1rem auto 3rem;
}
.article-txt * {
  border: revert;
  margin: revert;
  padding: revert;
}
.article-txt h2, .article-txt h3, .article-txt h4,
.article-txt h5, .article-txt h6 {
  color: var(--dark-grey);
}
.article-txt h2 {
  font-size: 1.7rem;
  margin: 3rem 0 1rem;
}
.article-txt h3 {
  font-size: 1.3rem;
  margin: 2.2rem 0 1rem;
}
.article-txt li {
  margin: 0.2rem 0 0.2rem -1rem;
}
.article-txt pre {
  background: var(--light-grey);
  border-radius: 0.4rem;
  font-family: var(--mono);
  margin: 1rem 0 2rem;
  overflow-x: auto;
  padding: 1rem 1rem 1.5rem;
  white-space: pre-wrap;
  word-wrap: break-word;
}
"""

  defTheme: string = "<!DOCTYPE html>\n" & html("\n",
    head("\n",
      meta(charset = "utf-8"), "\n",
      meta(name = "description", content = theme.exp.description), "\n",
      meta(name = "generator", content = app.name), "\n",
      meta(name = "viewport",
        content = "width=device-width, initial-scale=1.0"), "\n",
      "<title>" & theme.exp.title & "</title>", "\n"
    ), "\n",
    style("\n", defThemeCss), "\n",
    body("\n",
      nav(class = theme.css.nav, id = theme.css.navAnchor, "\n", theme.exp.nav),
        "\n",
      header(class = theme.css.header, theme.exp.header), "\n",
      main(class = theme.css.main, "\n", theme.exp.toc, "\n",
        theme.exp.articles, "\n"), "\n",
      footer(class = theme.css.footer, theme.exp.footer), "\n"
    ), "\n"
  )


proc print(str: string) =
  ## Echoes to stdout.
  echo str


proc saveFile(file: string, str: string, quitOnErr = true) =
  ## Writes a string to file.
  try:
    createDir(parentDir(unixToNativePath(file)))
    writeFile(unixToNativePath(file), str)
  except IOError, OSError, CatchableError:
    print(err.fileNotSaved)
    if quitOnErr:
      quit()


proc loadFile(file: string, quitOnErr = true): string =
  ## Reads a text file and returns the contents as a string.
  try:
    return readFile(unixToNativePath(file))
  except IOError, OSError:
    print(err.fileNotLoaded)
    if quitOnErr:
      quit()


proc loadJson[T](file: string, t: typedesc[T], quitOnErr = true): T =
  ## Reads a JSON file and returns the contents as the specified type.
  try:
    # Allow JSON metadata files to have missing keys.
    if $type(t) == $SourceMeta:
      return jsonTo(parseFile(unixToNativePath(file)), t,
        Joptions(allowMissingKeys: true))
    else:
      return jsonTo(parseFile(unixToNativePath(file)), t)
  except IOError, JsonParsingError, OSError:
    print(err.jsonNotLoaded)
    if quitOnErr:
      quit()


proc genConfigDir(path = getCurrentDir()) =
  ## Outputs a config directory with a default config and theme.
  saveFile(path & "/" & env.configDir & "/" & env.configFile,
    pretty(toJson(defConfig)))
  saveFile(path & "/" & env.configDir & "/" & env.themeFile, defTheme)


proc loadConfig(path = getCurrentDir()): (Config, string) =
  ## Reads the config and theme files and returns the values as a tuple and
  ## string respectively.
  try:
    let configDir = path & "/" & env.configDir & "/"
    return (loadJson(configDir & env.configFile, Config),
      loadFile(configDir & env.themeFile))
  except IOError:
    print(err.configNotFound)
    quit()
  except KeyError as error:
    print(err.configNotParsed)
    print(error.msg)
    quit()
  except OSError as error:
    print(error.msg)
    quit()


proc prepImage(file: string, dataUri = true): string =
  ## Loads an image file and returns the data as a base64-encoded string.
  let imgType =
    if endsWith(file, ".svg"):
      "svg+xml"
    else:
      split(file, ".")[^1]
  if dataUri:
    "data:image/" & imgType & ";base64, " & encode(loadFile(file))
  else:
    encode(loadFile(file))


proc parseMdToHtml(str: string): string =
  ## Performs a basic conversion of Markdown to HTML.
  # This works around rstToHtml() Markdown anomalies:
  # - Replace triple backticks with <code> tags instead of <p> tags.
  # - Treat double-space newlines as newlines instead of single spaces.
  # It also enables a few custom html tags, e.g. div and style.
  let
    preConvTags = @[
      ("```", "```{{-code-}}"),
      ("  \n", "{{-br-}}"),
      ]
    postConvTags = @[
      ("<p>{{-code-}}", "<pre><code>"), ("{{-code-}}</p>", "</code></pre>"),
      ("<p>{= style =}", "<style>"), ("{= /style =}</p>", "</style>"),
      ("{{-br-}}", "<br/>"), ("{= br =}", "<br/>"), ("{= br/ =}", "<br/>"),
      ("{= div class=&quot;", "<div class=\""),
      ("{= div id=&quot;", "<div id=\""),
      ("{= div style=&quot;", "<div style=\""),
      ("&quot; =}", "\">"), ("{= /div =}", "</div>"),
      ]
    html = multiReplace(rstToHtml(multiReplace(str, preConvTags),
      {RstParseOption.roSupportMarkdown}, newStringTable(modeStyleInsensitive)),
      postConvTags)
  html


proc prepText(file: string): string =
  ## Loads a text file, applies additional operations based on the file
  ## extension and returns the contents as a string.
  let
    ext = splitFile(file)[2]
    text = loadFile(file)
    # Escape angle brackets in plaintext format.
    replaceChars = @[("<", "&lt;"), (">", "&gt;")]
  case(ext)
  of ".html":
    "\n" & multiReplace(text, replaceChars) & "\n"
  of ".md":
    section(class = theme.css.articleTxt, "\n", parseMdToHtml(text), "\n") &
      "\n"
  else:
    pre(class = theme.css.articlePreTxt, "\n", multiReplace(text, replaceChars),
      "\n") & "\n"


proc pathHasImage(path: string): (bool, seq[string]) =
  ## Checks whether a directory contains an image. Returns `true` if the
  ## directory contains an image and a sequence of image paths, `false` and an
  ## empty sequence otherwise.
  let images = join(collect(for ext in env.ext.image:
    join(toSeq(walkFiles(unixToNativePath(path & "/*" & ext))), " ")), " ")
  if unicode.strip(images) != "":
    return (true, split(images, " "))
  else:
    return (false, @[])


proc addInlineImages(file: string, sources: seq[Source]): string =
  ## Replaces image variables in a text with images from sources, returning the
  ## contents as a string. Images should be located in the same directory as the
  ## metatext file which contains the image references.
  ## Example of an image variable: `{! image.png !}`
  let
    str = prepText(file)
    (_, imagePaths) = pathHasImage(splitFile(file)[0])
    # Look up the encoded images in the sources list.
    replaceImages = concat(
      # Inline images.
      collect(for s in sources:
        if s.path in imagePaths:
          let cssClass =
            if s.ext == ".svg": theme.css.articleImgSvg
            else: theme.css.articleImg
          (theme.image.inlineStart & extractFilename(s.path) &
            theme.image.inlineEnd, img(id = s.anchor[1..^1] & "-" &
            replace(toLower(s.name), " ", "-"), class = cssClass,
            src = s.content, alt = s.name))),
      # Background images.
      collect(for s in sources:
        if s.path in imagePaths:
          (theme.image.bgStart & extractFilename(s.path) &
            theme.image.bgEnd, "url(\"" & s.content & "\")"))
      )
  multiReplace(str, replaceImages)


proc getSourcePaths(path = getCurrentDir(), ignoreDirs: seq[string],
  ignoreFiles: seq[string]): seq[string] =
  ## Walks the project directory, gets a list of files and sorts the sequence
  ## alphabetically by top level directory name.
  let files = sorted(collect(for item in walkDirRec(path):
    let
      metaTextPath = unixToNativePath(splitFile(item)[0] & "/" & env.metaText)
      (hasImage, _) = pathHasImage(splitFile(item)[0])
      included =
        # Ignore directories in the ignoreDirs list.
        if extractFilename(splitFile(item)[0]) in ignoreDirs: false
        # Ignore files in the ignoreFiles list.
        elif toLower(extractFilename(item)) in ignoreFiles: false
        # Ignore the output file.
        elif item == unixToNativePath(path & "/" & env.outputFile): false
        # Ignore unrecognised formats.
        elif not (toLower(splitFile(item)[2]) in env.ext.text) and
          not (toLower(splitFile(item)[2]) in env.ext.image): false
        # Ignore the source text for images when a metadata text file exists.
        elif (toLower(splitFile(item)[2]) in env.ext.text) and
          fileExists(metaTextPath) and hasImage: false
        else: true
    # Ignore the config directory.
    if not contains(item, "/" & env.configDir) and included: item),
      system.cmp[string])
  files


proc getSources(path = getCurrentDir(), files: seq[string]): seq[Source] =
  ## Looks in a path for source files and returns a sequence of the file
  ## contents.
  let sources: seq[Source] = collect(for n in 0..(len(files) - 1):
    let
      (fPath, fName, fExt) = splitFile(files[n])

      prevPath =
        if n == 0: ""
        else: splitFile(files[n - 1])[0]
      nextPath =
        if n == (len(files) - 1): ""
        else: splitFile(files[n + 1])[0]
      fSerial =
        if prevPath == fPath or nextPath == fPath: true
        else: false
      fFirst =
        if prevPath != fPath: true
        else: false
      fLast =
        if nextPath != fPath: true
        else: false

      metaPath = unixToNativePath(fPath & "/" & env.metaFile)
      metaTextPath = unixToNativePath(fPath & "/" & env.metaText)

    var fMeta: SourceMeta
    # Load metadata file in the same directory as the source.
    if fileExists(metaPath):
      fMeta = loadJson(metaPath, SourceMeta, quitOnErr = false)
    # Fill in any missing fields.
    if isNone(fMeta.title):
      fMeta.title = some(replace(replace(fName, "_", " "), "-", " "))
    if isNone(fMeta.author):
      fMeta.author = some("")
    if isNone(fMeta.showTitles):
      fMeta.showTitles = some(true)
    if isNone(fMeta.inlineImages):
      fMeta.inlineImages = some(false)

    let
      # Set title by order of availability: individual item filename as title
      # for items part of a series, title from metadata, Fall back to title
      # based on the item filename.
      fTitle =
        if fSerial:
          replace(replace(fName, "_", " "), "-", " ")
        else:
          get(fMeta.title)

      fReplaceChars = @[("\"", ""), ("'", ""), (",", ""), (".", ""),
        (" - ", "-"), (" ", "-")]
      fAnchorBase = toLower(join(["#", replace(replace(fPath, path & "/", ""),
        "/", "-")]))
      # Anchor uses the main/series title, not individual item titles.
      fAnchorName = toLower(multiReplace(get(fMeta.title), fReplaceChars))
      fAnchor =
        if not endsWith(fAnchorBase, fAnchorName): fAnchorBase & "-" &
          fAnchorName
        else: fAnchorBase

      fContent =
        if fExt in env.ext.image:
          prepImage(files[n])
        # Meta text file contents takes precedence over source text if found.
        elif fExt in env.ext.text:
          if fileExists(metaTextPath):
            prepText(metaTextPath)
          else:
            prepText(files[n])
        else:
          ""

      src: Source = (
        anchor: fAnchor,
        author: get(fMeta.author),
        content: fContent,
        ext: fExt,
        first: fFirst,
        inlineImages: get(fMeta.inlineImages),
        last: fLast,
        name: fName,
        path: files[n],
        serial: fSerial,
        series: get(fMeta.title),
        showTitles: get(fMeta.showTitles),
        title: fTitle,
        )
    src)
  sources


proc genNav(urls: seq[Url]): string =
  ## Generates the main navigation contents as HTML string.
  let nav = join(collect(for u in urls:
    a(class = u.class, href = u.href, title = u.title, u.source) & "\n"
    ))
  nav


proc genHeader(str: string, path = getCurrentDir()): string =
  ## Generates header contents as HTML string. It will attempt to find a header
  ## image in the config directory. If a header image is found, the header
  ## string will be used as the alt text. Otherwise, only the header string is
  ## included the header.
  for ext in env.ext.image:
    let imagePath = unixToNativePath(path & "/" & env.configDir & "/" &
      env.headerFile & ext)
    if fileExists(imagePath):
      return img(class = theme.css.articleImg, src = prepImage(imagePath),
        alt = str)
  return str


proc genToc(sources: seq[Source]): string =
  ## Generates a table of contents as HTML string.
  let toc = nav(class = theme.css.toc, id = theme.css.tocAnchor, "\n",
    h1(class = theme.css.tocTitle, theme.label.tocTitle), "\n",
    join(collect(for s in sources:
    if s.content != "" and s.author != "" and s.first:
      ul(class = theme.css.tocItem, "\n",
        li(class = theme.css.tocItemTitle,
          a(class = theme.css.tocLink, href = s.anchor, s.series)), "\n",
        li(class = theme.css.tocItemAuthor, s.author), "\n") & "\n"
    elif s.content != "" and s.author == "" and s.first:
      a(class = theme.css.tocLink, href = s.anchor, s.series) & "\n"
    ))) & "\n"
  toc


proc genArticles(sources: seq[Source]): string =
  ## Generates a section of articles as HTML string.
  let articles = section(class = theme.css.articles, "\n",
    join(collect(for s in sources:
    if s.content != "":
      let
        # Show the header if it is the first item.
        header =
          if s.first:
            header(class = theme.css.articleHeader, "\n",
              h1(class = theme.css.articleTitle, s.series), "\n",
              p(class = theme.css.articleAuthor, s.author), "\n") & "\n"
          else:
            ""

        # Include meta text if it exists for the first image.
        metaTextPath = unixToNativePath(splitFile(s.path)[0] & "/" &
          env.metaText)
        metaText =
          # No inline images.
          if s.first and fileExists(metaTextPath) and
            (s.ext in env.ext.image) and not s.inlineImages:
            prepText(metaTextPath) & "\n"
          # With inline images.
          elif s.first and (s.ext in env.ext.image) and s.inlineImages:
            addInlineImages(metaTextPath, sources) & "\n"
          else: ""

        # Show the title if the series title is different from the source
        # title. Hide titles if inline images is enabled.
        title =
          if s.showTitles and toLower(s.series) != toLower(s.title) and
            not s.inlineImages:
            h2(class = theme.css.articleTitle, s.title) & "\n"
          else:
            ""

        ct =
          if s.ext in env.ext.image and not s.inlineImages:
            let cssClass =
              if s.ext == ".svg": theme.css.articleImgSvg
              else: theme.css.articleImg
            img(class = cssClass, src = s.content, alt = s.name)
          # Do not set an image tag if inline images is enabled.
          elif s.ext in env.ext.image and s.inlineImages:
            ""
          else:
            s.content

        # Show the footer if it is the last item.
        footer =
          if s.last or s.inlineImages:
            nav(class = theme.css.articleNav, "\n", theme.label.articleNav,
              a(class = theme.css.navLink, href = "#" & theme.css.navAnchor,
                theme.label.nav), "\n",
              a(class = theme.css.navLink, href = "#" & theme.css.tocAnchor,
                theme.label.toc), "\n") & "\n"
          else:
            ""

      # Hide other images in a series if inline images is enabled.
      if s.ext in env.ext.image and s.inlineImages and not s.first:
        ""
      else:
        article(class = theme.css.article, id = s.anchor[1..^1], "\n", header,
          metaText, title, ct, footer, "\n") & "\n"
  )))
  articles


proc genHtmlFile(path = getCurrentDir()) =
  ## Saves the HTML file.
  let
    (config, themeHtml) = loadConfig(path)
    nav = genNav(config.nav)
    header = genHeader(config.header, path)
    sources = getSources(path, getSourcePaths(path, config.ignoreDirs,
    config.ignoreFiles))
    toc = genToc(sources)
    articles = genArticles(sources)
    replaceVars = @[
      (theme.exp.app, app.name),
      (theme.exp.articles, articles),
      (theme.exp.description, config.description),
      (theme.exp.footer, config.footer),
      (theme.exp.header, header),
      (theme.exp.nav, nav),
      (theme.exp.title, config.title),
      (theme.exp.toc, toc),
    ]
    htmlStr = multiReplace(themeHtml, replaceVars)
  saveFile(path & "/" & env.outputFile, htmlStr)


proc getArgs(): seq[string] =
  ## Reads the command line arguments and returns them in a string sequence.
  var argParser = initOptParser(quoteShellCommand(commandLineParams()))
  collect(for kind, arg, _ in argParser.getopt():
    if kind == cmdArgument: arg)


proc getProjectPath(args: seq[string]): string =
  ## Gets the project path from a sequence of command line arguments. If no
  ## additional args are given, use the current directory as the project path.
  if len(args) == 2:
    if dirExists(args[1]):
      args[1]
    else:
      print(err.dirNotExists)
      quit()
  else:
    getCurrentDir()


proc showHelp() =
  ## Prints the help options to stdout.
  print("Usage: " & app.name & " [option] [path]\n\nOptions:\n")
  for opt in help:
    print(opt.cmd & " " & opt.params & "\t" & opt.desc)


proc showVersion() =
  ## Prints the app version to stdout.
  print(app.name & " " & app.version)


proc main() =
  ## Maps commands to functions.
  let
    args = getArgs()
    projPath = getProjectPath(args)

  case(len(args))
  of 1..2:
    case(args[0])
    of "n", "new":
      genConfigDir(projPath)
    of "m", "make":
      genHtmlFile(projPath)
    of "v", "version":
      showVersion()
    else:
      showHelp()
  else:
    showHelp()


main()
